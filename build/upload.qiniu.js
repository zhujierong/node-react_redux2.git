const qiniu = require('qiniu')
const appConfig = require('../configs/app.config')
const Promise = require('core-js/features/promise')
const fs = require('fs')
const path = require('path')

const CDN = appConfig.cdn

let mac = new qiniu.auth.digest.Mac(CDN.accessKey, CDN.secretKey);

//自定义凭证有效期（示例2小时，expires单位为秒，为上传凭证的有效时间）
let options = {
    scope: CDN.bucket,
    expires: CDN.expires,
    // 自定义返回的JSON格式的内容
    // returnBody: '{"key":"$(key)","hash":"$(etag)","fsize":$(fsize),"bucket":"$(bucket)","name":"$(x:name)"}',
};
let putPolicy = new qiniu.rs.PutPolicy(options);
let uploadToken = putPolicy.uploadToken(mac);

let config = new qiniu.conf.Config();
// 空间对应的机房
config.zone = qiniu.zone.Zone_z2;
// 是否使用https域名
//config.useHttpsDomain = true;
// 上传是否使用cdn加速
//config.useCdnDomain = true;
const excludeFiles = ['index.html', 'index.pug', 'server-entry.js']
let filePath = '../dist';

function upload(localFile, key) {
    let formUploader = new qiniu.form_up.FormUploader(config);
    let putExtra = new qiniu.form_up.PutExtra();
    // let key = 'test.mp4';

    return new Promise((resolve, reject) => {
        // 文件上传
        formUploader.putFile(uploadToken, key, localFile, putExtra, function (respErr,
            respBody, respInfo) {
            if (respErr) {
                throw respErr;
            }
            if (respInfo.statusCode == 200) {
                // console.log(respBody);
                resolve(respBody)
            } else {
                // console.log(respInfo.statusCode);
                // console.log(respBody);
                reject(respBody)
            }
        });
    })
}

let promiseArr = []
function readFileAll(filePath, dir) {
    const _filePath = dir ? `${filePath}/${dir}` : filePath
    const files = fs.readdirSync(path.join(__dirname, _filePath))
    files.map(fileName => {
        const _path = dir ? `${dir}/${fileName}` : fileName
        // console.log('========================================')
        // console.log('========================================')
        // console.log('========================================')
        // console.log('fileName', fileName)
        // console.log('_filePath', _filePath)
        // console.log('_path', _path)
        // console.log(path.join(__dirname, _filePath, _path))
        let stat = fs.lstatSync(path.join(__dirname, _filePath, fileName))
        let isDir = stat.isDirectory();
        console.log('isDir', fileName, isDir)
        if (isDir) {
            const arr = readFileAll(filePath, _path);
            promiseArr = promiseArr.concat(arr)
        } else {
            if (excludeFiles.indexOf(fileName) === -1) {
                promiseArr.push(upload(
                    path.join(__dirname, '../dist', _path),
                    _path
                ))
            } else {
                console.log('无需上传cdn的文件：', _path)
            }
        }
    })
}

readFileAll(filePath)

Promise.all(promiseArr).then(resps => {
    console.log('cdn upload success', resps)
}).catch(err => {
    console.log('cdn upload error：', err)
})
