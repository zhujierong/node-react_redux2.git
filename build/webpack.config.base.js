const path = require('path')
const isDev = process.env.NODE_ENV !== 'production'
const NODE_ENV = process.env.NODE_ENV || 'production'

module.exports = {
    mode: NODE_ENV,
    output: {
        path: path.join(__dirname, '../dist'),
        publicPath: isDev ? '/public/' : ''
    },
    // devtool: 'inline-source-map',
    resolve: {
        extensions: ['.js', '.jsx'],
        // alias: {
        //     react: path.resolve('./node_modules/react')
        // }
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                use: 'babel-loader',
                exclude: [
                    path.join(__dirname, '../node_modules')
                ]
            },
            // {
            //     test: /\.s[ac]ss$/i,
            //     use: [
            //         "style-loader",
            //         "css-loader",
            //         "sass-loader",
            //     ],
            // },
        ],
    },
}
