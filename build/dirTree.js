// 项目目录结构输出工具
const fs = require("fs");
const path = require("path");
const rootDir = path.resolve(__dirname, '../');

const createDirTree = (dir, skip) => {

    console.log('+—— root');
    const recur = (curDir) => {
        fs.readdirSync(curDir, { withFileTypes: true }).forEach((value) => {
            if (skip.includes(value.name)) return;
            const childPath = path.join(curDir, value.name);
            const depths = childPath.replace(rootDir, '').split(path.sep).filter(Boolean).length;
            const isDirectory = value.isDirectory();
            const prefix = `${'| '.repeat(depths - 1)}${isDirectory ? '+' : ' '}—— `;
            console.log(`${prefix}${value.name}`);
            if (isDirectory) {
                recur(path.join(curDir, value.name));
            }
        });
    }
    recur(dir);
}

createDirTree(rootDir, ["node_modules", ".vscode", ".git"])

// +—— root
//  —— .editorconfig
//  —— .eslintrc.js
//  —— .gitignore
//  —— babel.config.js
// +—— build
// |  —— dirTree.js
// |  —— webpack.config.base.js
// |  —— webpack.config.client.js
// |  —— webpack.config.server.js
// |  —— webpack.config.todoapp.js
// +—— client
// |  —— .eslintrc.js
// | +—— config
// | |  —— router.js
// |  —— index.html
// |  —— index.js
// |  —— index.pug
// |  —— server-entry.js
// | +—— store
// | | +—— actions
// | | |  —— const.js
// | | |  —— index.js
// | | |  —— user.js
// | |  —— index.js
// | | +—— reduces
// | | |  —— index.js
// | | |  —— user.js
// | +—— utils
// | |  —— http.js
// | +—— views
// | |  —— app.js
// | | +—— login
// | | |  —— adapter.js
// | | |  —— container.js
// | | |  —— index.js
// | |  —— router.js
// +—— configs
// |  —— app.config.js
//  —— favicon.ico
//  —— nodemon.json
//  —— package-lock.json
//  —— package.json
// +—— public
// | +—— api
// +—— server
// | +—— apis
// | |  —— index.js
// |  —— index.js
// | +—— util
// | |  —— dev-static.js
// | |  —— handle-login.js
// | |  —— proxy.js
// | |  —— renderString.js










// 参考格式
// │  ├─yargs
// │  │  ├─lib
// │  │  ├─locales
// │  │  └─node_modules
// │  │      ├─find-up
// │  │      ├─locate-path
// │  │      ├─p-locate
// │  │      └─path-exists
// │  ├─yargs-parser
// │  │  ├─lib
// │  │  └─node_modules
// │  │      └─camelcase
// │  ├─ylru
// │  └─yocto-queue
// ├─public
// │  └─api
// └─server
//     ├─apis
//     └─util
