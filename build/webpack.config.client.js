const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const webpack = require('webpack')
const WebpackMerge = require('webpack-merge')
const WebpackBase = require('./webpack.config.base')
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const appConfig = require(path.join(__dirname, '../configs/app.config.js'))
const isDev = process.env.NODE_ENV !== 'production'

const config = WebpackMerge.merge(WebpackBase, {
    entry: {
        app: path.join(__dirname, '../client/index.js'),
    },
    output: {
        // filename: 'static/js/[name].[chunkhash].js',
        filename: '[name].[chunkhash].js',
    },
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '../client/index.html'),
            favicon: path.join(__dirname, '../favicon.ico')
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '../client/index.pug'),
            favicon: path.join(__dirname, '../favicon.ico'),
            filename: 'index.pug'
        }),
        new MiniCssExtractPlugin({
            filename: 'static/css/[name].style.[chunkhash].css',
        })
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // "style-loader",
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.jpg$/],
                loader: require.resolve('url-loader'),
                options: {
                    name: 'static/img/[name].[hash:8].[ext]',
                },
            },
            {
                exclude: [/\.(js|jsx)$/, /\.(html|pug)$/, /\.(css|s[ac]ss)$/, /\.json$/, /\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: require.resolve('file-loader'),
                options: {
                    name: 'static/media/[name].[hash:8].[ext]'
                }
            },
        ],
    },
})

if (isDev) {
    config.devServer = {
        host: '0.0.0.0',
        port: '3030',
        contentBase: path.join(__dirname, '../dist'),
        hot: true, // 开启hot module replacement
        overlay: {
            errors: true //出现错误时，在网站上显示
        },
        publicPath: '/public/', //添加
        historyApiFallback: {
            index: '/public/index.html'
        },
        proxy: {
            '/api': 'http://localhost:3031'
        }
    }
    config.plugins.push(
        // new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        // new ESLintPlugin()
    )
    config.entry = {
        app: [
            'react-hot-loader/patch',
            path.join(__dirname, '../client/index.js'),
        ]
    }
} else {
    config.entry = {
        app: path.join(__dirname, '../client/index.js'),
    }
    config.optimization = {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin()
        ],
        splitChunks: {
            chunks: 'initial',
            cacheGroups: {
                common: {
                    test: (module) => {
                        let result = /[\\/]node_modules[\\/]/.test(module.context)
                        return result
                    },
                    name: 'common',
                    priority: 10,
                },
                reactBase: {
                    test: (module) => {
                        // const suffix = path.basename(module.context)
                        const curPath = path.parse(module.context)
                        const cwd = process.cwd()
                        const suffix = curPath.dir.replace(cwd, '')
                        const result = /react|redux|prop-types/.test(suffix)
                        // console.log('----------------------reactBase--', suffix, result)
                        return result
                    },
                    name: "reactBase",
                    priority: 30,
                },
            }
        }
    }
    config.plugins.push(
        new CompressionWebpackPlugin({
            test: /\.(js|css)$/,
            // test: /\.(js|css|html|svg)$/,
        })
    )
    // config.output.filename = '[name].[chunkhash].js'
    config.output.publicPath = appConfig.cdn.domain
}
module.exports = config
