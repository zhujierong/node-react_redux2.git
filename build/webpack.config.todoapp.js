const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const ESLintPlugin = require('eslint-webpack-plugin')
const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const webpackBase = require('./webpack.config.base')

const isDev = process.env.NODE_ENV == 'development'

const config = webpackMerge.merge(webpackBase, {
    mode: 'development',
    entry: {
        app: path.join(__dirname, '../todoApp/index.js'),
    },
    output: {
        filename: '[name].[hash].js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '../todoApp/index.html'),
            favicon: path.join(__dirname, '../favicon.ico')
        }),
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
        ],
    },
})

if (isDev) {
    config.devServer = {
        host: '0.0.0.0',
        port: '3030',
        contentBase: path.join(__dirname, '../dist'),
        hot: true, // 开启hot module replacement
        overlay: {
            errors: true //出现错误时，在网站上显示
        },
        publicPath: '/public/', //添加
        historyApiFallback: {
            index: '/public/index.html'
        },
        proxy: {
            '/api': 'http://localhost:3031'
        }
    }
    config.plugins.push(
        // new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        // new ESLintPlugin()
    )
    config.entry = {
        app: [
            'react-hot-loader/patch',
            path.join(__dirname, '../todoApp/index.js'),
        ]
    }
}

module.exports = config
