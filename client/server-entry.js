import React from 'react'
import { Provider } from 'react-redux'
import createStoreMap from './store/index'
import { StaticRouter } from 'react-router-dom'
import App from './views/app'

// ----------------------------------
export default (stores, context, path) => {
    return (
        < StaticRouter context={context} location={path} >
            <Provider store={stores}>
                <App />
            </Provider>
        </StaticRouter >
    )
}

export { createStoreMap }
