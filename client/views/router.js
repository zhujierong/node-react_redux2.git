
import React from 'react'
import { Route, Switch } from 'react-router'
import LoginPage from './login'

export default class Routers extends React.Component {
    constructor() {
        super()
    }
    render() {
        return (
            <Switch>
                <Route path='/login' component={LoginPage}></Route>
            </Switch>
        )
    }
}
