import { requestLogin, requestUserInfo } from '../../store/actions/user'

const mapStateToProps = state => state
const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => {
            dispatch(requestLogin(data))
        },
        getUserInfo: (data) => {
            dispatch(requestUserInfo(data))
        }
    }
}

export {
    mapStateToProps,
    mapDispatchToProps
}
