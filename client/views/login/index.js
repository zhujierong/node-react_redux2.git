import React from 'react'
import { connect } from 'react-redux'
import { mapDispatchToProps, mapStateToProps } from './container'

class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            tel: '18743534543',
            inputCode: '123456',
        }
    }

    componentDidMount() {

    }

    handleChange(e) {
        let controll = e.target
        this.setState({
            [controll.getAttribute('name')]: controll.value
        })
    }

    login() {
        this.props.login({
            ...this.state,
            inputCode: "1234",
            originCode: "api",
            tel: "18743534543",
            type: "person",
        })
    }

    getUserInfo() {
        this.props.getUserInfo({
            xxx: '111'
        })
    }

    render() {
        return (
            <div>
                <input name="tel" placeholder="账号" onChange={this.handleChange.bind(this)} value={this.state.tel} />
                <input name="inputCode" placeholder="验证码" onChange={this.handleChange.bind(this)} value={this.state.inputCode} />
                <button type="button" onClick={this.login.bind(this)}>登录</button>
                <br />
                <button type="button" onClick={this.getUserInfo.bind(this)}>用户信息</button>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Login)
