import {
    ATYPE
} from '../actions/user'
import { REQUSET_STATUS } from '../actions/const';

let initState = {
    isLogin: false,
    request: {
        login: REQUSET_STATUS.IDLE
    }
}

function userinfo(state = initState, action) {
    switch (action.type) {
        case ATYPE.LOGIN:
            return {
                ...state,
                request: {
                    ...state.request,
                    ...action.request
                },
                data: action.data,
                isLogin: action.isLogin,
            }
            break;
        case ATYPE.LOGOUT:
            return {
                ...state,
                request: {
                    ...state.request,
                    ...action.request,
                },
                data: action.data,
                isLogin: action.isLogin,
            }
            break;

        default: return state
            break;
    }
}


export {
    userinfo,
}
