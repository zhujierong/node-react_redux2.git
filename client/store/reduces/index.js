import { combineReducers } from 'redux'

// import {
//     ATYPE,
//     VisibilityFilters
// } from '../actions'
import { userinfo } from './user'

// function todos(state = [], action) {
//     let newState
//     switch (action.type) {
//         case ATYPE.ADD_TODO:
//             return [
//                 ...state,
//                 {
//                     text: action.text,
//                     completed: false
//                 }
//             ]
//         case ATYPE.EDIT_TODO_COMPLETED:
//             newState = [
//                 ...state,
//             ]
//             newState = newState.map((item, ii) => {
//                 if (ii === action.index) {
//                     return {
//                         ...item,
//                         completed: action.completed
//                     }
//                 }
//                 return item
//             })
//             return newState
//         default:
//             return state
//     }
// }

// const visibilityFilter = (state = VisibilityFilters.SHOW_ALL, action) => {
//     switch (action.type) {
//         case 'SET_VISIBILITY_FILTER':
//             return action.filter
//         default:
//             return state
//     }
// }

export default combineReducers({
    // todos,
    // filter: visibilityFilter,
    userInfo: userinfo
})
