import { applyMiddleware, createStore } from 'redux'
import storeRecuces from './reduces'
import thunk from 'redux-thunk'

const createStoreMap = (__INITIAL_STATE__) => {
    // return createStore(storeRecuces, __INITIAL_STATE__)
    return createStore(storeRecuces, __INITIAL_STATE__, applyMiddleware(thunk))
}

export default createStoreMap
