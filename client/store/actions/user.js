
import http from '../../utils/http'
import { REQUSET_STATUS } from './const'

const ATYPE = {
    REQUIRE_LOGIN: 'REQUIRE_LOGIN',
    REQUIRE_LOGOUT: 'REQUIRE_LOGOUT',
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT',
}

function login(data) {
    return {
        type: ATYPE.LOGIN,
        data,
        request: {
            login: REQUSET_STATUS.SUCCESS
        },
        isLogin: true,
    }
}

function logout(data) {
    return {
        type: ATYPE.LOGOUT,
        data,
        request: {
            logout: REQUSET_STATUS.SUCCESS
        },
        isLogin: false,
    }
}

function _requestLogin(status) {
    return {
        type: ATYPE.REQUIRE_LOGIN,
        status
    }
}
function _requestLogout() {
    return {
        type: ATYPE.REQUIRE_LOGOUT,
        status
    }
}

// 登录接口
function requestLogin(data) {
    return (dispatch) => {
        dispatch(_requestLogin(REQUSET_STATUS.LOADING))
        http.post('/login', data)
            .then(resp => {
                dispatch(login(resp))
            }).catch((err) => {
                dispatch(_requestLogin(REQUSET_STATUS.ERROR))
            })
    }
}
function requestUserInfo(data) {
    return (dispatch) => {
        http.post('/userInfo', data)
            .then(resp => {
            }).catch((err) => {
            })
    }
}

// // 退出登录
// function requestLogout(data) {
//     return function (dispatch) {
//         dispatch(_requestLogout(REQUSET_STATUS.LOADING))
//         http.post('/login', data)
//             .then(resp => {
//                 dispatch(logout(resp))
//             }).catch((err) => {
//                 console.log('/login', err)
//                 dispatch(_requestLogout(REQUSET_STATUS.ERROR))
//             })
//     }
// }


export {
    ATYPE,
    login,
    logout,
    requestLogin,
    requestUserInfo,
    // requestLogout,
}
