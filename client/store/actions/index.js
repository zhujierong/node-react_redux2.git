export const ATYPE = {
    ADD_TODO: 'ADD_TODO',
    EDIT_TODO_COMPLETED: 'EDIT_TODO_COMPLETED',
    SET_VISIBILITY_FILTER: 'SET_VISIBILITY_FILTER'
}

export const addTodo = (text) => {
    return {
        type: ATYPE.ADD_TODO,
        text
    }
}

export const setVisibilityFilter = (filter) => {
    return {
        type: ATYPE.SET_VISIBILITY_FILTER,
        filter
    }
}

export const toggleTodo = (index, completed) => {
    return {
        type: ATYPE.EDIT_TODO_COMPLETED,
        index,
        completed
    }
}

export const VisibilityFilters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETED: 'SHOW_COMPLETED',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
}
