import axios from 'axios'
import appConfig from '../../configs/app.config'

const queryString = (url, params) => {
    const jsonStr = Object.keys(params).reduce((result, key) => {
        return result += `${key}=${params[key]}&`
    }, '')
    const suffix = url.indexOf('?') > 0 ? '&' : '?'
    return `${url}${suffix}${jsonStr.substr(0, jsonStr.length - 1)}`
}

const get = (url, params) => {
    return new Promise((resolve, reject) => {
        axios.get(queryString(`${appConfig.apis.baseUrl}api${url}`, params))
            .then(resp => {
                resolve(resp.data)
            })
            .catch(reject)
    })
}

const post = (url, data) => {
    return new Promise((resolve, reject) => {
        axios.post(`${appConfig.apis.baseUrl}api${url}`, data)
            .then(resp => {
                resolve(resp)
            })
            .catch(reject)
    })
}

export default {
    get,
    post
}
