module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module"
    },
    "extends": [
        "airbnb",
        // "plugin:react-hooks/recommended"
    ],
    "plugins": [
        "react-hooks"
    ],
    "rules": {
        "indent": ["error", 4],
        "react/jsx-indent": ["error", 4],
        "react-hooks/rules-of-hooks": "error", // 检查 Hook 的规则
        "react-hooks/exhaustive-deps": "warn", // 检查 effect 的依赖
        "semi": ["off"],
        "react/jsx-filename-extension": ["error", { "extensions": [".js", ".jsx"] }],
        "comma-dangle": ["error", "only-multiline"],
        // "react/destructuring-assignment": ["off"]
        "react/jsx-one-expression-per-line": ["off"],
        "react/no-unused-prop-types": ['off'],//检查prop是否定义而未被使用
        "react/require-default-props": ['off'], //props defaultProps 认证
        "arrow-parens": ['off'],
        "react/prop-types": ['off'],
        "react/jsx-indent": ['off'],
        "indent": ['off'],
        "react/destructuring-assignment": ['off'],
        "prefer-object-spread": ['off'],
        "arrow-body-style": ['off'],
        "react/jsx-first-prop-new-line": ['off'],
        "react/jsx-indent-props": ['off'],
        "jsx-a11y/click-events-have-key-events": ['off'],
        "jsx-a11y/no-noninteractive-element-interactions": ['off'],
        "react/no-array-index-key": ['off'],
        "react/jsx-props-no-spreading": ['off'],


    }
}
