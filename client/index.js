import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import createStoreMap from './store'
import App from './views/app'
import { BrowserRouter } from 'react-router-dom'
import './style/index.scss'

const store = createStoreMap(window.__INITIAL_STATE__)
const render = (Component) => {
    const domRender = module.hot ? ReactDOM.render : ReactDOM.hydrate
    domRender(
        <BrowserRouter>
            <Provider store={store}>
                <Component />
            </Provider>
        </BrowserRouter>,
        document.getElementById('root'),
    )
}

render(App)

if (module.hot) {
    module.hot.accept('./views/app', () => {
        const NextApp = require('./views/app').default // eslint-disable-line
        render(NextApp)
    })
}
