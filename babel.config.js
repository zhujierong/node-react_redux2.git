module.exports = function (api) {
    api.cache(true)
    const presets = [
        [
            "@babel/preset-env",
            {
                "corejs": "3",
                "useBuiltIns": "entry",
            }
        ],
        // '@babel/preset-stage-1', 官方弃用
        '@babel/preset-react',
    ]
    const plugins = [
        ["@babel/plugin-proposal-decorators", { "legacy": true }],
        'react-hot-loader/babel',
        'transform-async-to-generator',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-transform-runtime'
    ]
    return {
        presets,
        plugins
    }
}
