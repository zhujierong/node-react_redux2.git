const appConfig = require('../../configs/app.config')
const suffix = appConfig.apis.suffix

const apis = {
    prefix: '/api/',
    apiList: {
        login: {
            url: `${suffix}/api/store/layoutApi/login.json`,
            // parameter: ['id'] // 请求url中的参数 如: http://api/:id
            params: ['token', 'inputCode', 'originCode', 'tel', 'type',]
        },
        userInfo: {
            url: `${suffix}/api/store/account/getAccount.json`,
            // parameter: ['id'] // 请求url中的参数 如: http://api/:id
            params: ['token']
        }
    },
    // 获取api对象信息
    getItem(path = '') {
        if (path.indexOf(this.prefix) === -1) return
        const urlSuffixStr = path.split(this.prefix)[1]
        const apiUrls = urlSuffixStr.split('/')
        const apiUrlKey = apiUrls[0]
        const apiItem = this.apiList[apiUrlKey]
        return apiItem
    },
    // 获取apiList的key值数组，用于api router的使用
    getApis() {
        return Object.keys(apis.apiList).map(d => {
            let item = apis.apiList[d]
            if (item.parameter && item.parameter.length) {
                return `${d}/:${item.parameter.join('/:')}`
            }
            return `${d}`
        })
    },
    // 获取请求到服务器的url
    getUrl(path = '') {
        if (path.indexOf(this.prefix) === -1) return
        const urlSuffixStr = path.split(this.prefix)[1]
        const apiUrls = urlSuffixStr.split('/')
        const apiUrlKey = apiUrls[0]
        const parameterStr = urlSuffixStr.split(apiUrlKey)[1] || ''
        const apiItem = this.apiList[apiUrlKey]
        return `${apiItem.url}${parameterStr}`
    },
    // 获取URL中的/:params 参数
    getUrlParams(path = '') {
        if (path.indexOf(this.prefix) === -1) return
        let apiUrls = path.split(this.prefix)[1].split('/')
        const apiUrlKey = apiUrls[0]
        let result = {}
        const parameterArr = apiUrls.slice(1)
        const apiItem = this.apiList[apiUrlKey]
        apiItem.parameter && apiItem.parameter.map((d, ii) => {
            result[d] = parameterArr[ii]
        })
        return result
    },
}

module.exports = apis
