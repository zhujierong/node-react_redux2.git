const Axios = require('axios')
const appConfig = require('../../configs/app.config')
const path = require('path')
const apis = require(path.join(__dirname, '../apis'))
const baseUrl = appConfig.apis.domain

module.exports = async function (ctx, next) {
    console.log('-------------proxy-----------------login')
    const path = apis.getUrl(ctx.path)
    const query = ctx.query
    let session = ctx.session
    if (session.user) {
        ctx.body = session.user
        return
    }
    const params = {
        ...query,
        ...ctx.request.body
    }
    let _url = `${baseUrl}/${path}`
    console.log('post-url', _url)
    console.log('post-params', params)

    console.log('Axios')
    await Axios(_url, {
        data: params,
        method: ctx.method,
    }).then(res => {
        if (res.status === 200) {
            ctx.body = res.data
            // if 登录成功
            session.user = res.data.data
            // else
            // session.user = null
        } else {
            res.status = 500
            ctx.body = res.data
        }
    }).catch(err => {
        ctx.status = 500
        if (err.response) {
            ctx.status = err.response.status
            console.log(`axios ${ctx.method} ${ctx.response.status}: ${_url}`)
        } else {
            ctx.body = {
                success: false,
                msg: '未知错误'
            }
        }
    })
}












    // console.log('ctx', ctx)
    // const req = ctx.req
    // const res = ctx.res
    // const path = ctx.path
    // const user = ctx.session.user || {}
    // const query = ctx.query
    // const { needAccessToken } = query
    // const params = {
    //     ...query,
    //     ...ctx.request.body
    // }

    // // console.log('--------------------------------')
    // // console.log('req', req)
    // // console.log('path', path)
    // // console.log('session', user)
    // // console.log('query', query)
    // console.log('ctx.querystring', ctx.querystring)
    // console.log('ctx.header', ctx.header)
    // console.log('ctx.ip', ctx.ip)
    // console.log('params ', params)
    // // console.log('ctx.request ', ctx.request)
    // console.log('ctx.request.body ', ctx.request.body)

    // // console.log('--------------------------------')
    // if (needAccessToken && !user.accessToken) {
    //     ctx.status = 401
    //     ctx.body = {
    //         success: false,
    //         msg: '未登录'
    //     }
    // }
    // ctx.status = 200
    // ctx.body = {
    //     name: '张三'
    // }
    // axios.post(`${baseUrl}/accesstoken`, {
    //     accesstoken: user.accessToken
    // }).then(resp => {
    //     if (resp.status === 200 && resp.data.success) {
    //         req.session.user = {
    //             accessToken: req.body.accessToken,
    //             loginName: resp.data.loginname,
    //             id: resp.data.id,
    //             avatarUrl: resp.data.avatar_url
    //         }
    //         res.json({
    //             sucess: true,
    //             data: resp.data
    //         })
    //     }
    // }).catch(err => {
    //     if (err.response) {
    //         res.json({
    //             success: false,
    //             data: err.response.data
    //         })
    //     } else {
    //         next(err)
    //     }
    // })
