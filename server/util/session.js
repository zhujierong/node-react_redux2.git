
const crypto = require('crypto');
const session = require('koa-session')

function init(app) {
    const hash = crypto.createHash('sha1');
    const hashKey = hash.digest('hex')
    // session 配置信息
    const CONFIG = {
        key: hashKey, /**  cookie的key。 (默认是 koa:sess) */
        maxAge: 24 * 60 * 60 * 1000,   /**  session 过期时间，以毫秒ms为单位计算 。*/
        autoCommit: true, /** 自动提交到响应头。(默认是 true) */
        overwrite: true, /** 是否允许重写 。(默认是 true) */
        httpOnly: true, /** 是否设置HttpOnly，如果在Cookie中设置了"HttpOnly"属性，那么通过程序(JS脚本、Applet等)将无法读取到Cookie信息，这样能有效的防止XSS攻击。  (默认 true) */
        signed: true, /** 是否签名。(默认是 true) */
        rolling: true, /** 是否每次响应时刷新Session的有效期。(默认是 false) */
        renew: false, /** 是否在Session快过期时刷新Session的有效期。(默认是 false) */
    };

    app.keys = [hashKey];
    app.use(session(CONFIG, app))
}

// 从session中获取参数
function getSessionUserParams(session, params) {
    let result = {}
    if (!session || !params) return result
    if (!session.user) return result
    params.map(d => {
        if (session.user[d]) result[d] = session.user[d]
    })
    return result
}


module.exports = {
    init,
    getSessionUserParams,
}
